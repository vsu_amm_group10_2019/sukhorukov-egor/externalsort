﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VsualSorfWF
{
	public partial class Form1 : Form
	{
		public int[] array;

		public Form1()
		{
			InitializeComponent();
		}

		private void button1_Click( object sender, EventArgs e )
		{
			Random random = new Random();
			array = new int[(int) countElements.Value];
			array = array.Select( x => random.Next( (int) minLim.Value, (int) ( maxLim.Value + 1 ) ) ).ToArray();
			int k=0;
			for (int j = 1; array.Length > (Math.Pow(3, j)-1) / 2; j++)
            {
				k = j;
            }
			var d = (Math.Pow(3, k) + 1) / 2;
			while (d >= 1)
			{
				for (var i = d; i < array.Length; i++)
				{
					var j = i;
					while ((j >= d) && (array[(int)(j - d)] > array[(int)j]))
					{
						Swap(ref array[(int)j], ref array[(int)(j - d)]);
						j = j - d;						
					}
					drawSort(array, -1);
					drawMarking();
					Thread.Sleep(300);
				}
				k--;
				d = (Math.Pow(3, k) + 1) / 2;
			}
		}

		private void Swap(ref int a, ref int b)
		{
			var t = a;
			a = b;
			b = t;
		}

		private void drawSort(int[] array, int a)
		{
			bool flag = true;
			Pen pen = new Pen( Color.DarkOrange );

			Graphics graphics = pictureBox1.CreateGraphics();
			graphics.Clear( Color.Black );
			for ( int i = (int)minLim.Value; i <= maxLim.Value; i++ )
			{
				for ( int j = 0; j < array.Length; j++ )
				{
					{
						if (flag)
							pen = new Pen(Color.CornflowerBlue);
						else
							pen = new Pen(Color.DarkOrange);
						flag = !flag;
					}
					if ( array[ j ] >= i )
						graphics.FillRectangle( new SolidBrush( pen.Color ), 15 * j, pictureBox1.Height - 15 * i, 15, 15 );
				}
			}
		}

		private void drawMarking()
		{
			Graphics graphics = pictureBox1.CreateGraphics();

			Pen pen = new Pen( Color.DarkGreen );
			for ( int i = 0; i < pictureBox1.Height; i += 15 )
				graphics.DrawLine( pen, 0, pictureBox1.Height - i, pictureBox1.Width, pictureBox1.Height - i );
			for ( int i = 0; i < pictureBox1.Width; i += 15 )
				graphics.DrawLine( pen, i, 0, i, pictureBox1.Width );
		}

		private void pictureBox1_Paint( object sender, PaintEventArgs e )
		{
			((PictureBox)sender).CreateGraphics().Clear( Color.Black );
			drawMarking();
		}
	}
}
